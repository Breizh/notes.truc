/* ----------========== [ Pointeur/Reference/Variables ] ==========---------- //
[ Make ]

g++ -o main truc.cpp -Wall

methode moche :
si .cpp existe pas en crée un avec

#ifndef FICHER_H
#define FICHER_H

#endif

[ Variable ]
type maVariable = valeur; // JG - si type primitif ; si type est une Classe -> appel au constructeur

maVariable = valeur variable
&maVariable = @ variable

[ Pointeurs ]
type * monPointeur = @;

monPointeur = @
*monPointeur = valeur de la variable a l'@ du pointeur

adresse = 64bits == bits os

[ References ]
La référence est un pointeur qui s'utilise comme une variable.
On "oublie" que c'est un pointeur.
(lien symbolique shell)

int & maReference = ma variable; // JG - uniquement dans la spécification d'une méthode
 // JG - la variable référencée doit exister (donc déclarée et initialiser avant)

maReference = valeur variable
&maReference = @ variable

[ Vector ]*/
#include <vector>

std::vector<int> v; // JG - appel implicte au constructeur par défaut (qui existe pour vector)

v.push_back(1); // Ajoute 1 à la fin
v.pop_back();   // Enlève le dernière element

void permuteEntiers(int & a, int & b){
    int c = a;
    a = b;
    b = c;
}

int main(){
    int i =5;
    int j =6;
    permuteEntiers(i, j);
    return 0;
}

/*
[ Const, static, virtual ]

Const : rend la variable constante (non modifiable)
Static : rend la variable commun a tout les objets // JG - accessible à tous les objets de la même classe
Virtual : rend la methode surchargable // JG - nécessaire dans la classe mère quand on veut redéfinir la méthode dans la classe fille pour que le bon type soit retrouvé lors de l'exécution (dans le cas de structures de données polymorphes)

Ordre Static Const pour la lisibilité

[ Const ]

--------------------+----------------------------+---------------------+---------------------------+
Déclaration         | "Décodage"                 | Pointeur modifiable | Valeur pointée modifiable |
--------------------+----------------------------+---------------------+---------------------------+
const int * p       | p est un pointeur sur      | oui                 | non                       |
                    | un entier constant         |                     |                           |
--------------------+----------------------------+---------------------+---------------------------+
int * const p       | p est un pointeur          | non                 | oui                       |
                    | constant sur un entier     |                     |                           |
--------------------+----------------------------+---------------------+---------------------------+
const int * const p | p est un pointeur constant | non                 | non                       |
                    | sur un entier constant     |                     |                           |
--------------------+----------------------------+---------------------+---------------------------+
*/

/* ------------================ [ Entrer/Sortie ] ================------------ */

int variable, variable1, variable2;

#include <iostream>

using namespace std;    // Pour ne pas avoir a préfixer avec std::

cout << "giga text" << variable << endl;    //endl == "\n" // JG - si la valeur de la variable doit être affichée il faut l'initilaiser (sinon tu auras à l'affichage ce qu'il y a à l'@ &variable ... 0 si tu as de la chance)

cin >> variable1 >> variable2;

// --------------================= [ Classe ] =================-------------- //

// === Fichier point2D.h === //

#ifndef POINT2D_H
#define POINT2D_H

#include <library>

class Point2D{
public:

    // ----- Forme canonique de copien ----- //
    Point2D(int x=X_DEFAUT,int y=Y_DEFAUT); //constructeur
    Point2D(const Point2D & unPoint);
    Point2D & operator = (const Point2D & unPoint);
    ~Point2D();                             //destructeur

    // ----- Methode ----- //
    int getX()const;
    int getY()const;
    void deplacerDe(int dx,int dy);

    void afficher(std::ostream & sortie = std::cout)const;

private:
    int m_x;
    int m_y; // m_ permet d'identifier (m = membre de la classe)

    static const int X_DEFAUT;
    static const int Y_DEFAUT;

}; // <-- point virgule
#endif

// === Fichier point2D.cpp === //

#include "Point2D.h"
#include <iostream>

using namespace std;

Point2D::Point2D(int x,int y)
: /*Si heritage*/ConstructeurClasserMere(parametres),
/*Si objet*/m_objet(parametre, constructeur, objets), // JG - ? m_objet(paramètres) pour l'appel au constructeur adéquat sinon tentative d'appeler le constructeur par défaut m_objet() ... qui doit exister !
m_x(x), m_y(y){}

Point2D::~Point2D(){}

// getters et setters
//[                 ]

const int Point2D::X_DEFAUT = 0;
const int Point2D::Y_DEFAUT = 0;

//Fin class

static function // => permet d'appeler la fonction sans avoir d'objets

// ===== Template ===== //

// cf TP02/conteneur.h

// ===== heritage ===== //

class Point3D : public Point2D{

};

// public/protected/private cf cours 4 chap 8  diapo 22 // JG - mode d'héritage classique = public

// ===== instanciation d'objets ===== //

#include "Point2D.h"
Point2D p1;     // Allocation statique // JG et appel du constructeur par défaut ok ici car tu as mis des constantes dans la définition du constructeur du .h (jamais le faire dans le .cpp)

int main(){
    Point2D p2(20, 30);  // Allocation Automatique

    Point2D * p3 = new Point2D(20, 30);   // Allocation Dynamique
    // Paramètres optionnel en cas de valeur par defaut

    p2.methode();

    p3->methode(); // JG ou sinon (*p3).methode()

    delete p3;  //aloué dans le tas // JG donc libérer la mémoire ! inutile pour p2 alloué dans la pile
}

// ------------================== [ Try/catch ] ==================------------

try{
  // code...
}
catch(...){ // JG - le catch prend en compte le TYPE de l'exception récupérée (pointeur ou objet)
  cout << "erreur indéfinie \n";
}

// ===== example =====

//Classe normal .h
class VectInt {
public:
  VectInt (unsigned inttaille) ;
  virtual~VectInt () ;
  int& operator[] (unsigned inti);

private :
  unsigned int m_taille ;
  int* m_adr ;
  staticconstint TAILLEMAX; // taille max autorisée pour un vecteur // JG - static const int ..
};

//classe erreur .h
class IndiceIncorrect : public exception{
  public:
    unsigned int m_indice ; // indice qui provoque l'exception (attribut public ! )
    IndiceIncorrect(intindice) { m_indice = indice ; }
    inline const char * what() const noexcept {return ("Erreur sur l'indice\n");}
};

//.cpp
int & VectInt::operator [] (unsigned inti) {
  if(i>=m_taille) {
    throw IndiceIncorrect(i) ;// levée exception    // JG ou throw new IndiceIncorrect(i) et dans ce cas dans le catch (voir après)
  }
  return m_adr[i] ;             // fonctionnement normal
}

try{
  VectInt v(10) ;
  v[11] = 5 ;  // indice trop grand => exception levée
  v[0]  = 0;// Cette instruction ne sera pas exécutée...
}
catch(exception & e) { // gestionnaire d'exception // si throw new ... -> catch (IndiceIncorrect * p)
  cout << e.what();
  exit (EXIT_FAILURE);
}

// ===== fin example ===== //
// --------------================= [ Tesr Unitaire ] =================-------------- //

CPPUNIT_ASSERT_MESSAGE(message, condition);              // Vérifier qu'une condition est vrai
CPPUNIT_ASSERT_EQUAL_MESSAGE(message, attendue, valeur); // Vérifier une egalité
CPPUNIT_ASSERT_THROW_MESSAGE(message, expression);       // Vérifier qu'une expression lève une exception d'un type donnée
CPPUNIT_ASSERT_NO_THROW_MESSAGE(message, expression);    //Vérifier qu'une expression ne lève aucune une exception

/* ---------------================== [ UML ] ==================---------------

Classe A⬥----ClasseB

Composition == Appartien a l'objet, meurt avec l'objet (noir = mort) Example : Nez

Dans ClasseA
Cardinalité 1 = ClasseB
Cardinalité 0..1 = ClasseB *
Cardinalité 0..* = vector<ClasseB>

Classe A⬦----ClasseB

Agregation == ne meurt pas avec l'objets (blanc = nuage) Example : chapeaux

Dans ClasseA
Cardinalité 1 = ClasseB &
Cardinalité 0..1 = ClasseB *
Cardinalité 0..* = Vector<ClasseB *>
*/
