/* ----------==================== NOTE LANGAGE C ====================---------- */

// -----========== Generale ==========----- //
/*
[ Variable ]
type maVariable = valeur;

maVariable = valeur variable
&maVariable = @ variable

[ Pointeurs ]
monPointeur = @
*monPointeur = valeur de la variable a l'@ du pointeur

adresse = 64bits == bits os

[ Type ]

================================

char = int convertis en ascii

char chaine[]   => Valeur du tableau
char* chaine    => Pointeur de la chaine
char* chaine[]  => Pointeur du tableau
char** chaine   => Pointeur du tableau

char * S1 = "ab";
S1 ++;            //rajoute +1 a d'adresse

*S1 == 'b'
*(S1+1) == 'b'
*(S1 + n) == S1[n]

char S2[] = "a";
//!\ S2 ++ pas possible /!\\

================================*/

//--------------------------//
typedef struct structure
{
  unsigned char* un ;
  unsigned int deux ;
  unsigned int trois ;
} nomStruct ;
//--------------------------//

nomStruct my_struct;

my_struct.un = 50;
my_struct.trois = 51;


nomStruct* my_structB;

my_structB->un = 50;
my_structB->trois = 51;

//   ___  _   _
//  / _ \| | | |
// | (_) | |_| |
//  \___/ \__,_|
//

//--------------------------//
typedef struct structure
{
  unsigned char* un ;
  unsigned int deux ;
  unsigned int trois ;
};
//--------------------------//

struct structure my_struct;

my_struct.un = 50;
my_struct.trois = 51;


struct structure* my_structB;

my_structB->un = 50;
my_structB->trois = 51;

/*================================

+--------------------------------------------------------+
| Type         |  Minimum  | Maximum  | Nombre d'octetes |
+--------------+-----------+----------+------------------+
| char         |-128       | 128      | 1                |
+--------------+-----------+----------+------------------+
|short         |-32768     | 32768    | 2                |
+--------------+-----------+----------+------------------+
| int          |-32768     | 32768    | 4                |
+--------------+-----------+----------+------------------+
| long         |-2147483648|2147483648| 8                |
+--------------+-----------+----------+------------------+
|unsigned char |0          | 255      | 1                |
+--------------+-----------+----------+------------------+
|unsigned short|0          | 65535    | 2                |
+--------------+-----------+----------+------------------+
|unsigned int  |0          | 65535    | 4                |
+--------------+-----------+----------+------------------+
|unsigned long |0          |4294967295| 8                |
+--------------+-----------+----------+------------------+

[ Pré-processeur ]*/

#define begin {
#define end }
int main()
begin   // => begin = {

/* code */

end     // => end = }

//----- macro commande -----//
// Les macors commandes sont moins lourde que les fonction ou les procedure.

#define carre(x) (x*x)  // retourne le carré de x

#define COMPARE(A, B) ((A > B) ? A : B)   //retourne la valeur la plus grande
// (condition) ? valeur si vrai : valeur si faux

//  \ sur plusieur ligne

#define AFFICHER_FONCTION if(DEBUG) {  \
    printf("%s\n", __func__);          \
    }

/* [ MAKEFILE ]
nom : makefile
commande : make

cible: pré-requis
  commande


[ operation ]


val = val << 2 ; // décale val de 2 bits à gauche. Par exemple, %00100001 devient %10000100
// ou bien
val <<= 2 ;


// Remarque : le décalage à droite se note >> avec la même syntaxe

[ fichier ]



AccessMode :
    "r" (pour read) ouverture en lecture. Le fichier doit exister.
    "w" (pour write) ouverture en écriture. Si le fichier existe, il sera alors remplacé.
    "a" (pour append) ouverture en ajout. Si le fichier n'existe pas, il sera alors créé.
*/
// example

FILE* fichier;
fichier = fopen("guadalest_copie.pgm", AccessMode);
fprintf(fichier, "text %s", varible);
fclose(fichier);

//[ main ]

int main(int argc, char const *argv[])
/*
argc = nombre d'argument
argv[] = string argument
argv[0] = nom du programme
*/

// -----========== String/char ==========----- //
char chaine[20] = ("String");
char chaine[20] = {'S','r','i','n','g','\0'};   // \0 = fin de chaine

strlen(chaine); // nombre de charactère dans la chaine = 6 (\0 n'est pas compté)

// -----========== printf()/scanf(); ==========----- //
//affichage variable
printf("%i", int);    // %x = Variable
/*
+---------------------------------------------------+
|SYMBOLE |  Type  | IMPRESSION COMME                |
|--------+--------+---------------------------------|
|%d ou %i|int     |Entier relatif                   |
|--------+--------+---------------------------------|
|%u      |int     |Entier naturel                   |
|--------+--------+---------------------------------|
|%o      |int     |Entier en octal                  |
|--------+--------+---------------------------------|
|%x      |int     |Entier en Hexa                   |
|--------+--------+---------------------------------|
|%c      |int     |Caractères                       |
|--------+--------+---------------------------------|
|%f      |Double  |Réel en décimale                 |
|--------+--------+---------------------------------|
|%.2f    |Double  |Réel 2 chiffre après la virgules |
|--------+--------+---------------------------------|
|%e      |Double  |Réel en notation scientifique    |
|--------+--------+---------------------------------|
|%s      |Char[]  |Chaîne de caractères             |
+---------------------------------------------------+

+----+----------------+
|\n  |Saut ligne      |
+----+----------------+
|\r  |Retour chariot  |
+----+----------------+
|    |                |
+----+----------------+
*/
printf("%4i", 123);  //==>  _123      (_ = espace)
printf("%4i", 1234); //==>  1234
printf("%4i", 12345);//==>  12345

printf("quelquechose"); //==> quelquechose %
printf("quelquechose \n"); //==> quelquechose

scanf("%s\n", chaine);    // Ne lirt pas une chaîne qui contient des espaces

printf("%i\n", 0x0F); //= 15

// -----========== math.h ==========----- //
pow(value, power);


// -----========== string.h ==========----- //

void * malloc( size_t memorySize );

/*
Cette fonction permet d'allouer un bloc de mémoire dans le tas (le heap en anglais).
Attention : la mémoire allouée dynamiquement n'est pas automatiquement relachée.
Il faudra donc, après utilisation, libérer ce bloc de mémoire via un appel à la fonction free.

/!\ Attention /!\
ne pas oublier free(pointeur) pour libérer l'espace
*/

// == example == //

char my_str[] = "chaine de carractère":

char* my_copy;

my_copy = malloc(sizeof(char) * (strlen(my_str) + 1));
//alloue la taille d'un caractères x la taille de la chaine + '\0'


strcpy(my_copy,my_str); //copy my_str a l'emplacement pointé par my_copy

free(my_copy);

// ============= //
