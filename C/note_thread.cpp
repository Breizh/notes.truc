/*
====== Principe =====

Les thread permettent une execution asycrone d'une fonction

-- Les thread partage -- :

en espace mémoire :                   dans le descripteur :
  - text (code)                         - PID, PPID, ...
  - data (variables globales            - la gestion des signaux
  -                                     - les fichiers ouverts

-- Les thread ne partage pas -- :

en espace mémoire :                   dans le descripteur :
  - leur pile (variables locales)       - TID, ... (PID pour les threads)
  -                                     - la copie des registres(PC+SP)
*/

// =========== Création de thread ========== //

#include <thread>

// Création de thread avec std::thread::thread()
std::thread t(fn, arg1, arg2, ... );
/*
fn est un objet appelable :
  - fonction
  - objet d’une classe avec opérateur()

fn et arg, ... sont copiés avant d’être invoqués
*/

// Fonction
void fn (int& arg1, int arg2){
  arg1 = arg1 + arg2;
}

// Classe avec operateur()
class Fn {
  public :
    Fn(int& arg1_,int arg2_)
    : arg1(arg1_), arg2(arg2_) {}

    void operator()() { arg1 = arg1 + arg2; }
  private :
    int& arg1;
    int arg2;
};

// Création de thread par l'operateur() d'une Classe
int a=0, b=1;
std::thread t( Fn(a,b) );

// Création de thread par fonction
int a=0, b=1;
std::thread t(fn, a, b);

// Pour que le thread principale attende le thread t
t.join();

// Pour que le thread principale n'attende pas le thread t
t.detach();


// -- Création des threads dans un vecteur -- //
#include <vector>

vector<thread> vt;
vt.push_back(thread(fn_sans_ref, a, b));
vt.push_back(thread( Fn(a, b) ));
vt.at(0).join();
vt.at(1).join();


// =========== Verrou ========== //
/*
verrouiller():
  si loquet == fermé
  alors bloquer le thread en queue de FIFO
  finsi loquet = fermé

déverrouiller():
  loquet = ouvert
  si liste FIFO non vide
  alors libérer le thread en tête de FIFO
  finsi
*/

// ----- mutex ----- //

#include <mutex>

std::mutex mon_mutex;

mon_mutex.lock();

//. . . section critique . . .

mon_mutex.unlock();

/*
non copiable (comme std::thread)
non movable(un std::thread l’est)

FIFO non garanti : famine temporaire possible
Si un thread déverrouille un mutex qu’il n’a pas verrouillé : comportement indéfini
Si un thread verrouille à nouveau un mutex qu’ila déjà verrouillé : comportement indéfini (deadlock ou exception)
*/

// ----- lock_guard ----- //

#include <mutex>
using namespace std;

mutex mon_mutex;
{//début de portée
  lock_guard<mutex> verrou(mon_mutex);

  //. . . section critique . . .

}//fin de portée


// ----- unique_lock ----- //

#include <mutex>
using namespace std;
mutex mon_mutex;
{// début de portée
  unique_lock<mutex> verrou(mon_mutex);

  // . . . section critique . . .

  verrou.unlock()

  // . . . hors section critique . . .

}// fin de portée


// ------ lock() ----- //
/*
Utilité :
fonction permettant de verrouiller plusieurs mutex sans interblocage
utilisable avec std::mutex ou std::unique_lock
*/

#include <mutex>
using namespace std;

mutex mon_mutex1, mon_mutex2;

lock(mon_mutex1, mon_mutex2);

//. . . section critique pour les deux ressources . . .

mon_mutex1.unlock(); mon_mutex2.unlock();


// ------ Variable std::condition_variable ----- //
/*
Peut faire attendre plusieur threads au même endroit
Outils de synchronisation

En théorie condition_variable = FIFO
En pratique FIFO non assuré
*/

class SCN {
public :
  void entrer(void);
  void sortir(void);

private :
  const unsigned long nb_libre; //nb places
  std::mutex mut;
  std::condition_variable attente;
}

void SCN::entrer(void) {
  unique_lock<mutex>verr(mut);
  while(nb_libre == 0) {
    attente.wait(verr);
  }
  nb_libre--;
}

void SCN::sortir(void) {
  lock_guard<mutex>verr(mut);

  nb_libre++;
  attente.notify_one();
}
