# ========== python ========== #

# ----- Compilation ----- #
#
# python [nom du fichier]


# ----- General ----- #
# --- bonne pratique ---
#variable en minuscule

# ----- liste -----
ma_liste = []

# ----- try/catch -----
try:
    print(1 + "2")
except TypeError:
    print("Erreur, impossible")

# ----- I/O ----- #
print("hello world !")

nom = type(input("Quelle est ton nom : ")) #par defaut string
print("Bonjour", nom, sep = "_")    #par defaut retour a la ligne, end = "" pour enlever
print("Bonjour" + nom)
print("Bonjour {}".format(s))   #int + string = sucide
# > hello world !
# > Quelle est ton nom : Elian
# > Bonjour_Elian
# > BonjourElian
# > Bonjour Elian

nb = 5

print(type(nb))
# > <class 'int'>

# ----- Boucle/if ----- #
for i in range(5):  #si i on s'en fous, replacer par _
    print(i)
#i reste après le for
#range([debut inclue, fin exlue, pas])

#for each
fruits = ["apple", "banana", "cherry"]
for x in fruits:
  print(x)

while [condition]:
    print("While")

if condition:
    pass
elif condition:
    pass
else:
    pass


# ----- POO ----- #
#self = this
#include de classe
from Guerrier import Guerrier, Chef

# Example

class Guerrier:
    #Attribut       # __ = private. __ fait partie du nom

    #Attribut public
    nom = None
    pv = 0
    force = 10

    #Constructeur
    def __init__(self, nom):
        #Attribut dans le constructeur = attribut privé
        self.__PVMAX = 100   # Pas de constante a proprement parlé

        
        self.nom = nom
        self.pv = self.__PVMAX

    #Getteur et setteur
    def getNom(self):
        return self.nom

# Heritage
class Chef(Guerrier):

    def __init__(self, nom):
        super(Chef, self).__init__(nom)

Eliott = Chef("Eliott")

Eliott2 = Eliott #pointeur de l'objet

Eliott3 = Eliott[:] #copie de l'objet
