;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; compilation :
;
; nasm -f elf64 I-3-prog-1.asm
; gcc -fno-pie -no-pie -o I-3-prog-1 I-3-prog-1.o
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


; ========== Déclaration de constantes ========== ;

EXEMPLE_1 equ 52
EXEMPLE_2 equ 0xABCD

; ========== Déclaration de variables initialisées ========== ;

section .data
exemple_b db -1          						;; b = 8 bits 	=> 1 octets
exemple_w dw 512         						;; w = 16 bits	=> 2 octets
exemple_d dd 0xAABBCCDD  						;; d = 32 bits	=> 4 octets
exemple_q dq 3.14        						;; q = 64 bits	=> 8 octets

exemple_tab dw 56,65000    						;; tableau de 'dw'	=> exemple_tab = [56, 65000]

exemple_chai; 	Entiers
; 	Déclarer un string avec '%d' dedans
lea rdi, [msg]
mov rsi, valeur_a_afficher      ;; affectation sur les 64 bits de poids faible (sd)
mov rax, 0
call printf  		;; 0xa = \n et 0 = 0 terminal
MSG_LEN equ $ - exemple_chaine         			;; constante égale au nb de caracteres (avec le 0)

; ========== Déclaration de variables non initialisées ========== ;

section .bss
exemple_v1 resb 53 								;; réserve 53 octets 		(b = 8 bits)
exemple_v2 resw 10 								;; réserve 10 * 2 octets 	(w = 16 bits)


; ========== Zones d'instructions ========== ;

section .text
   global main, fonction_exemple:function 		;; toute les fonction à rajouter ici

; ========== fonction_exemple ========== ;
;
; arguments:
;  arg No 0 (rdi) : nombre entier
;
;  return (rax) : nombre entier

fonction_exemple:
 	; Sauvegarde du pointeur de pile
   	push rbp
   	mov  rbp, rsp

  ;	Code

    ; Restauration du pointeur de pile
   	pop rbp
   	ret


; ========== Ordre des registres pour les arguments d'appel de fonction ========== ;

;	Entiers ou pointeurs
 	1 : rdi
 	2 : rsi
 	3 : rdx
 	4 : rcx
 	5 : r8
 	6 : r9

;	Flottants
	1 : xmm0
	2 : xmm1
	3 : xmm2
	4 : xmm3
	5 : xmm4
	6 : xmm5
	7 : xmm6
	8 : xmm7


; ========== Pour afficher des valeurs (printf)  ========== ;

; 	Entiers
; 	Déclarer un string avec '%d' dedans
lea rdi, [msg]
mov rsi, valeur_a_afficher      ;; affectation sur les 64 bits de poids faible (sd)
mov rax, 0
call printf

; 	Flottants
; 	Déclarer un string avec '%f' dedans
lea rdi, [msg]
mov rsi, valeur_a_afficher      ;; affectation sur les 64 bits de poids faible (sd)
mov rax, 0
call printf

; ========== Résumé des instruction et des branchement  ========== ;

; addition
; eax = eax + ebx
add rax, rbs

; multiplication
; rbx = rax * rbx
mul rbx

; lea
; monPointeur = &maVariable
; maValeur = *monPointeur
lea rdi, msg
lea rdi, [msg]


;+-----------+--------------------+----------------+
;| Condition | Entiers non signés | Entiers signés |
;+-----------+-------------------------------------+
;|     ==    |                   JE                |
;+-----------+-------------------------------------+
;|     !=    |                   JNE               |
;+-----------+-------------------------------------+
;|     >     |     JA ou JNBE     |   JG ou JNLE   |
;+-----------+--------------------+----------------+
;|     >=    |     JAE ou JNB     |   JGE ou JNL   |
;+-----------+--------------------+----------------+
;|     <     |     JB ou JNAE     |   JL ou JNGE   |
;+-----------+--------------------+----------------+
;|     <=    |     JBE ou JNA     |   JLE ou JNG   |
;+-----------+--------------------+----------------+
