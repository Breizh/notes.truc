#include "classe.h"

Classe::Classe(string nom, int nb)
: m_nom(nom), m_nb(nb) {}

void Classe::affiche() {
  cout << "Nom = " << getNom() << " | Nb : " << getNb() << endl;
}
