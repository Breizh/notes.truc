# notes.truc

Compilation de note/cours sur les sytaxe et principe des differents langages

Repo organiser comme suit
```console
.
├── C
│   ├── note.c
│   ├── note.cpp
│   └── note_thread.cpp
├── LaTeX
│   ├── note.aux
│   ├── note.fdb_latexmk
│   ├── note.fls
│   ├── note.log
│   ├── note.out
│   ├── note.pdf
│   ├── note.synctex.gz
│   ├── note.tex
│   └── note.toc
├── Makefile
│   ├── 1erMakefile
│   │   ├── Makefile
│   │   ├── main
│   │   ├── main.cpp
│   │   └── main.o
│   ├── Makefile
│   └── makefile-classe
│       ├── Makefile
│       ├── classe.cpp
│       ├── classe.h
│       ├── classe.h.gch
│       └── main.cpp
├── README.md
├── note.asm
├── note.js
├── note.php
├── note.py
├── note.rs
└── note.sh

5 directories, 29 files
```
