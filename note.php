<?php
/*

// -----===== [ Commande d'interpretation ] =====----- //
php [nomProgramme]  => execute dans le shell
Option :
-l pour tester la syntaxe
-s pour voir ce que le code fait
-a [without nomProgramme] interpreteur php

-S [ip]:[port]  lance un serveur local


// -----===== [ Variables ] =====----- //
    Absence de typage des variables.
    Absence de déclaration préalable d'une variable.
    Obligation pour un nom de variable de débuter par le symbole '$'.

%d =/ %i

$A = 0;// Entier
$B = 'TEST';// Chaine
$C = 0.567654;// Flottant
$D = TRUE;// Booléen
*/

// -----===== [ tableau ] =====----- //

// Tableau associatif
$tab = array ('prenom' => 'toto',
              'age' => 10,
              'note' => 0
            );

// Tableau indexé
$tab = array('pomme','poire','pêche');

$tab[0]= 'pomme';
$tab[1]= 'poire';
$tab[2]= 'pêche';

$tab[]= 'pomme';
$tab[]= 'poire';
$tab[]= 'pêche';


// Tableau multidimensionnel

$matrice = array ( array(1,2), array(3,4), array(4,5));

// Fonction pour les tableau prédéfinies

count($tab)       // compte le nombre d'éléments
array_sum($tab)   // somme de tous les éléments
array_shift($tab) // retourne la première valeur et réduit de 1 le tableau
array_pop($tab)   // retourne la dernière valeur et réduit de 1 le tableau
ksort($tab)       // trie le tableau selon ses clés
sort($tab)        // trie les elements d'un tableau



// -----===== [ Print ] =====----- //
print("lalala");
$a = 1;
printf("lalala %d", $a);
print("lalala $a");
// lalala 1
print('lalala $a');
//lalala $a
print("lalala {$a}_3");
//lalala 1_3
print("lalala $a_3");
//error


echo "string";    //Shell ==> syntaxe bizzare ?

print("string");  //PHP ==> plus simple ?

printf("string"); //C ==> %i, %s...

var_dump($T);     //affiche tout le tableau

/*
[ Caractère spécieux ]

\n Fin de ligne
\r Retour à la ligne
\t Tabulation horizontale
\v Tabulation verticale
\e échappement
\f Saut de page


La double cotte ("") interprete ce qu'il y a dedans (\, $...)
La simple cotte ('') n'interpete rien et renvoie en chaine tout ce qui a entre les cotte

caractère dechappement anti slash \

/!\ ne pas confondre \n et <br>
*/

// -----===== [Super veriable] =====----- //

if(isset($_GET["val"])) {
  $val = $_GET["val"];
}

//si l'url fini par "?val=10" $val sera égale à 10

if ($_POST['val'] != '') {
  $val = $_POST['val'];
}

// -----===== [ Suoer globale ] =====----- //
// $GLOBALS   Information sur toute les variable super global

// $_SERVER   Information sur le serveur

// $_GET      Variable GET (dans l'url)
// $_POST     Variable POST

// $_FILES    Array des element uploadé

// $_COOKIE   Variable de COOKIE
// $_SESSION  Variable de session
// $_REQUEST
// $_ENV      Variables de l'environnement d'exécution de l’interpréteur php

// -----===== [ boucle ] =====----- //

for ($i=0; $i < ; $i++) {
  // code...
}

while (/*condition*/) {
  // code...
}

foreach ($variable as $key => $value) {
  // code...
}

// -----===== [ fonction ] =====----- //

function nom_fonction (type param_1, type &param_2, ..., type param_n]): type {
  /*instruction_1
  ...
  instruction_m;
  */
  [return $resultat;]
}
// Le paramètre 1 est donné par copie
// Le paramètre 2 est donné par référence

// -----===== [  Programmation Orienté Objet  ] =====----- //

class Voiture {
  private string $marque;
  private string $modele;

  function __construct(string $ma, string $mo) {
    $this->marque = $ma;
    $this->modele = $mo;
  }
}

$v = new Voiture('Renault','Kangoo');

// -----===== [ balise d'echapenet ] =====----- //
// Affichage dans de l'html
<?= "Hello world !\n" ?>
// La valeur de $A est <?= $A ?> !
<?php if ($expression == true): ?>
// Ceci sera affiché si l'expression est vrai.
<?php else: ?>
// Sinon, ceci sera affiché.
<?php endif; ?>

// -----===== [ fichier ] =====----- //

$dir = opendir('/chemin');
$contenue = readdir($dir);

$file = fopen('filename', 'mode');
/* mode :
'r' READ
'w' WRITE
'a' APPEND FILE*/
$line = fgets($file); // lit egalement "\n"
$newline = rtrim($line); // enlève les caractère de fin de ligne

explode ( string 'le separateur' , string $string [, int $limit = 'bn element max' ] ) : array

pathinfo($contenue)['atribut'];
/* atribut : de /www/htdocs/inc/lib.inc.php
['dirname']
['basename']
['extention']
['filename']
=
/www/htdocs/inc
lib.inc.php
php
lib.inc
*/

$bool = feof($file);

dirname(__FILE__) // retourne le dossier dans le quel se trouve ce fichier

// -----===== [ Session ] =====----- //
session_start();
 $_SESSION['USER'] = "admin";
session_write_close();

session_readonly(); //Lecture uniquement
$user =  $_SESSION['USER']


session_destroy(); // Supression de la session et de ses donnée

// -----===== [ sécurité ] =====----- //
htmlspecialchars("<script>alert('SPAM');<script>")

//> &lt;script&gt;alert('SPAM');&lt;script&gt;

// penser au === au besoin

// -----===== [ Acces à une BDD ] =====----- //
/*
PDO = PHP Data Object = interface pour accéder à la BDD


Etape d'acces à la BDD
       +-----------+
       | Ouverture |
       +-----------+
             |
  +---------------------+
  | Séléction de la BDD |
  +---------------------+
              |
        +-----------+
        |  Requête  |
        +-----------+
              |
+----------------------------+
| Exploitation des résultats |
+----------------------------+
              |
        +-------------+
        |  fermeture  |
        +-------------+
*/
// [ Ouverture ]

//Le chemin vers le fichier de BDD (pas l'url)
$database = 'sqlite:'.__DIR__.'/../data/bricomachin.db';

$db = new PDO($database);

// [ Requête ]

$db->exec($req) //ne renvoie pas de valeur (UPDATE, INSERT, DELETE)

$db->query($req) // Pour les SELECT, renvoie une istance de PDOStatement

$req="select * from michel";
$sth=$dbh->query($req);

fetchAll() //retourne l'ensemble des données
fetch()    //:permet une lecture séquentielle du résultat

$result=$sth->fetchAll(PDO::FETCH_ASSOC);
$result=$sth->fetchAll(PDO::FETCH_OBJ);
$result=$sth->fetchAll(PDO::FETCH_BOTH);

/* [ FETCH ]
fetch : Tableau de Tableau

PDO:FETCH_ASSOC
  Retourne un tableau associatif indexé par les noms de colonnes de la table de la BD

PDO:FETCH_NUM
  Retourne un tableau associatif indexé par des entiers à partir de 0 (No de colonne)

PDO:FETCH_BOTH
  Retourne un tableau associatif indexé par les noms de colonnes, mais aussi par les numéros des colonnes(mode par défaut

fetchAll : Tableau

PDO::FETCH_COLUMN
  Retourne une seule colonne de la table en précisant son No dans le paramètre suivant.

PDO::FETCH_NUM
  Retourne un tableau associatif indexé par des entiers à partir de 0 (No de colonne)

PDO::FETCH_BOTH
  Retourne un tableau associatif indexé par les noms de colonnes, mais aussi par les numéros des colonnes(mode par défaut)
*/

// [ Fermeture ]
/*
à la fin du script PHP, tout les objet sont detruit
*/

?>
