// ================ //
//  du HTML au JS   //
// ================ //
<script src="monScript.js"></script>
/*
à placer de préférence à la fin du <body>
pour ne pas retarder le chargement de la page dans certains navigateurs
*/

// Selection d'élement html
var boutons = document.getElementsByClassName("bouton_libre");
var titre = document.getElementById("titre"); //Un ID devant être unique, la méthode ne retourne qu'un seul élément

titre.setAttribute('onclick', 'maFonction()');


// ============== //
//    Variables   //
// ============== //

var ma_variable = 5;
var mon_tableau = ["pomme", "kiwi"];

const MA_CONSTANTE = 42;
// Si un objet est constant, on peut modifier l'objet mais pas la reference
const TAB = [12, 13, 14];
TAB.push(15);
// TAB = [12, 13, 14, 15]

//string
console.log("Ma constante = " + MA_CONSTANTE);
// -> Ma constante = 42
console.log(`Ma constante = ${MA_CONSTANTE}`)
// -> Ma constante = 42
console.log(`Ma constante = "${MA_CONSTANTE}"`)
// -> Ma constante = "42"

// --- Var et let ---
function abc() {
  var a = 1;
  {
    let b = 1;
    // 'a' est accessible ici
    // 'b' est accessible ici
  }
  // 'a' est accessible ici
  // en revanche 'b' ne l'est pas
}

/* --- Nom de Variables ---
le nom d'une variable js peut commencer par une *lettre* un "$" ou un "_" mais pas un chiffre
*/
var nomOkay, nom1Okay, nomOkay2, $nomOkay, _nomOkay; // => OK
var 1nomPasValide; // => SyntaxError !

// --- I/O ---
console.log("Mon message dans la console navigateur");
alert("pop up d'alerte");

var string = prompt("Entrez quelque chose :")
var i = parseInt(prompt("Entrez une valeur :"));

// --- tableau ---
var tab = [1, 111, 6, 3, 22];
// Tri "alphabétique" par défaut
tab.sort();
console.log( tab ); // => [1, 111, 22, 3, 6]

// Définition d'un tri "numérique"
tab.sort(function (a, b) {
  if (a < b) {return -1;}
  else if (a > b) {return1;}
  else {return0;}
});
console.log( tab ); // => [1, 3, 6, 22, 111]

var last = tab.pop(); // (last = 111) && supprime l'element
var first = tab.shift(); // (first = 1) && supprime l'element
console.log(tab); // [3, 6, 22]
tab.push(8);
tab.unshift(42);
console.log(tab); // [42, 3, 6, 22, 8]

// ============== //
//    Operateur   //
// ============== //
// les operateur son egale au PHP, il faut se mefier des type cependant;
console.log("10" + 5); // => 105
console.log("10" - 5); // => 5

// ============== //
//    function    //
// ============== //

function maFonction(variable) {
  console.log(variable);
}

// une fonction est un objet,
// on peut donc mettre des fonction en paramètre comme :

function lorem(x = 0) { return x*x; }

function ipsum(foo, bar) {
  if( typeof foo === 'function' ) {
    console.log("foo est une fonction et foo(bar) retourne : " + foo(bar));
  }
  else {
    console.log("foo n'est pas une fonction et a pour valeur : " + foo);
  }
}

// Ici on passe la fonction lorem en paramètre de la fonction ipsum
ipsum(lorem, 2);
// >>foo est une fonction et foo(bar) retourne : 4

// Ici on passe le résultat de la fonction lorem en paramètre de la fonction ipsum
ipsum(lorem(), 2);
// foo n'est pas une fonction et a pour valeur : 0


window.addEventListener("resize", () => {
  console.log(window.screen.width);
});


//======================
//|      Objets        |
//======================
var maVoiture = { marque:'Dacia', modele:'Sandero' };
// Format JSON (JavaScript Object Notation)
maVoiture.annee = 2012;
console.log( maVoiture.annee ); // 2012
console.log( maVoiture["annee"] ); // 2012

// Définition à la création de l'objet
var maVoiture = {
  marque: 'Dacia', modele: 'Sandero',
  getNom: function() {
    return this.marque+" "+this.modele;
  }
};


// Définition après-coup
maVoiture.getNom = function() {
  return this.marque + " " + this.modele;
};


// Ceci est un constructeur
// (une ancienne écriture qui reste toujours valable)
function Person(nick, age, sex, friends) {
  this.nick = nick;
  this.age = age;
  this.sex = sex;
  this.friends = friends;
}

var seb = newPerson('Sébastien', 23, 'm',[]);



/*
======================
|       AJAX         |
======================
Asychronous Javascript Ans XML
Asynchrone : communication client-serveur dans recharger la page
Javascript : appel AJAX et traitement des donnée en JS
XML : Format remplacé par le Json aujourd'hui

ensemble d'objets et de fonctionsmis à disposition par le langage JS,
afin d'exécuter des requêtes HTTP de manière asynchrone à un serveur web
*/

//exemple de requête GET

function ajaxGetRequest(callback, url, async) {
  var xhr = newXMLHttpRequest(); // Création de l'objet
  // Définition de la fonction à exécuter à chaque changement d'état
  xhr.onreadystatechange = function() {
    if (callback && xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
      // Si le serveur a fini son travail (XMLHttpRequest.DONE = 4)
      // et que le code d'état indique que tout s'est bien passé
      // => On appelle la fonction callback en lui passant la réponse
      callback(xhr.responseText);
    }
  };
  xhr.open("GET", url, async); // Initialisation de l'objet
  xhr.send(); // Envoi de la requête
}

//exemple de requête POST

function ajaxPostRequest(callback, url, async, data) {
  var xhr = newXMLHttpRequest(); // Création de l’objet
  xhr.onreadystatechange = function() {
    if (callback && xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
      callback(xhr.responseText);
    }
  };
  xhr.open("POST", url, async); // Initialisation de l’objet
  // Format des données (MIME) envoyées dans le corps de la requête HTTP
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xhr.send("data=" + encodeURIComponent(data)); // Envoi de la requête
}

/*
Pour les requêtes AJAX, la nouvelle norme étant de renvoyer une chaine au format JSON,
vous aurez besoin de "parser" cette chaine pour pouvoir manipuler les résultats comme un objet JS classique

transformer une chaine JSON en objets
*/

JSON.parse(""); // SyntaxError: Unexpected end of JSON input (/!\)
JSON.parse("{}"); // OKAY ! (un objet vide)
JSON.parse("[]"); // OKAY ! (un tableau vide)
JSON.parse('{ valeur : 42 }'); // SyntaxError: Unexpected token v ...
JSON.parse('{ "valeur" : 42 }'); // OKAY !
JSON.parse("[ 'val', 'test' ]"); // SyntaxError: Unexpected token ' ...
JSON.parse("[ \"val\", \"test\" ]"); // OKAY !

//clef avec toujours des double quote


/*
======================
|      JQuery        |
======================
import : https://code.jquery.com/

selecteur d'element :
pour la classe .bouton
*/

$(".bouton").val("valeur du bouton");
$(".bouton").attr("onclick", "maFonction(5)");
$(".bouton").css("color", "red");
