// ===== Cargo =====
/*
cargo new [nomprojet]     // crée un nouveau projet

cargo build               // compil
cargo run                 // run

pour l'ajout de lib, dans Cargo.toml
nomlib = "version"
*/

// ===== fonction de base =====

fn fonction() {
    println!("Hello, world!")
}

fn fonction_return() -> int {
    5               //ptdr ça return 5 si c'est la derrnière ligne
                    // Marche aussi avec les variables

    return 5;       // et la aussi MDR
}

println!("{:?}", variable); //{} pour les variable (osef du type)
                            //{:?} debeugage

use std::io;   // lib entrer sortie
use std::io::Write;

print!("Input : ");
io::stdout().flush().unwrap();
let mut chaine1 : String = String::new();
io::stdin().read_line(&mut chaine1);
println!("Output : {}", chaine1);


// ===== type =====

/*
par defaut constant
mut pour variable (mutable)
&variable reference
&mut variable referance mutable

Entier :

u unsigned ou i pour signed
pas obligé de le renseigner

de 8 a 128 pour le nombre de bit

+--------+-------+-------+
| type   | octet | nom   |
+--------+-------+-------+
| u/i8   |   1   |       |
+--------+-------+-------+
| u/i16  |   2   | short |
+--------+-------+-------+
| u/i32  |   4   | int   |
+--------+-------+-------+
| u/i64  |   8   | long  |
+--------+-------+-------+
| u/i128 |   16  |       |
+--------+-------+-------+
*/

let [nomVariable] : u8 = 5;

/*
Flotant :

f Flotant
32     ou 64
float     double
*/

let float : f32 = 5.5;

/*
caractère :
char

chaine :
str ou String       /!\ S majusule
str c'est chelou
*/

let char : char = '0';

let chaine2 : String = String::new(); // ou
let chaine3 : String::new();
let string : String = String::from("mange de l'eau");

// ===== if et loop =====

if string == "mange de l'eau" {
    print!("Alexandre ");
    println!("{}", string);
    println!("{:?}", string);   //affiche caractère d'echappement et ""
}

while true {
    break;
}

for i in 0..5 {
    println!("{:?}", i);
}

// ===== Objet =====
/*
Objet::methode

*/
