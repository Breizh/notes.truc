#ifndef CLASSE_H
#define CLASSE_H

#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

class Classe{
public:
  Classe(string nom, int nb);

  inline const string getNom()const   {return m_nom;};
  inline const int getNb()const   {return m_nb;};

  void affiche();

private:
  string m_nom;
  int m_nb;
};

#endif
