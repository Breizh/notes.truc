# ----------===== note shell =====---------- #


# ------====== conditions ======------ #

if comande ; then
  #code
else
  #code
fi

if comande
then
  #code
else
  #code
fi

# la condition evalue le code de retour d'une commmande

# [ ] commande test (man test)
# argument comment :
# -e file existe
# -d directory existe

# [[ ]] syntaxe bash

# ------====== print ======------ #

echo message

echo ----- commande -----

echo date -I
echo \' 'date -I'
echo \" "date -I"
echo \` `date -I`

echo ----- variable -----

echo $1
echo \' '$1'
echo \" "$1"
echo \` `$1`

echo ----- caractères séciaux -----

expensive=10

echo Prix du double slash \\ : $expensive
echo \' 'Prix du double slash \\ : $expensive'
echo \" "Prix du double slash \\ : $expensive"
echo \` `Prix du double slash \\ : $expensive`

#----- commande -----
#date -I
#' date -I
#" date -I
#` 2020-11-25
#----- variable -----
#lel
#' $1
#" lel
#./guillemets: ligne 15: lel : commande introuvable
#`
#----- caractères séciaux -----
#Prix du double slash \ : 10
#' Prix du double slash \\ : $expensive
#" Prix du double slash \ : 10
#./guillemets: ligne 24: Prix : commande introuvable
#`

# ------====== Variable ======------ #

#tout les variable sont des string

variable='michel'
#/!\ sans aucun espace

# Pour utiliser une variable on fait
$variable

# On peut aussi récuperer les argument on utilise
$1 # pour le 1er arguemt
$2 # Pour le 2eme argument

$((1+1)) #entre les parenthese ya des maths
